package addStringNumbers;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AddStringNumbersTest {

    @Test
    @DisplayName("Check is string empty or null")
    public void testIsEmpty() {
        int sumNull = AddStringNumbers.add(null);
        int sumEmpty = AddStringNumbers.add("");
        Assertions.assertThat(sumNull).isEqualTo(0);
        Assertions.assertThat(sumEmpty).isEqualTo(0);
    }

    @Test
    @DisplayName("Check if string contains a single number")
    public void testIfOneNumber() {
        int sum = AddStringNumbers.add("1");
        Assertions.assertThat(sum).isEqualTo(1);
    }

    @Test
    @DisplayName("Check is sum of two numbers comma or space delimited")
    public void testSumOfTwoNumbersCommaDelimited() {
        int sum = AddStringNumbers.add("2,5 5");
        Assertions.assertThat(sum).isEqualTo(12);
    }

    @Test
    @DisplayName("Check is sum of two numbers newline delimited without numbers above 1000")
    public void testSumOfTwoNumbersNewlineDelimited() {
        int sum = AddStringNumbers.add("3,2\n5 10 2005");
        Assertions.assertThat(sum).isEqualTo(20);
    }

    @Test
    @DisplayName("Check is sum of String")
    public void testSumString() {
        int sumString = AddStringNumbers.add("a");
        Assertions.assertThat(sumString).isEqualTo(0);
    }


}