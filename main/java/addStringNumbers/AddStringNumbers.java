package addStringNumbers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddStringNumbers {

    static int add(String numbers) {
        if (numbers == null || numbers.isEmpty()) {
            return 0;
        }
        int[] intNumbers = Arrays.stream(numbers.split("[,\\n\\s]+"))
                .map(s -> {
                    try {
                        return Integer.parseInt(s);
                    } catch (NumberFormatException e) {
                        return 0;
                    }
                })
                .mapToInt(n -> n).toArray();

        areNegativeNumbers(intNumbers);

        return Arrays.stream(intNumbers)
                .filter(a -> a <= 1000)
                .sum();
    }

    private static void areNegativeNumbers(int[] intNumbers) {
        List<Integer> listOfNegativeNumbers = new ArrayList<>();
        for (int number: intNumbers) {
            if (number < 0) {
                listOfNegativeNumbers.add(number);
            }
        }
        if (!listOfNegativeNumbers.isEmpty()) {
            throw new IllegalArgumentException("negatives not allowed: " + listOfNegativeNumbers);
        }
    }
}
